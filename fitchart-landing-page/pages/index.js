import Head from 'next/head'
import css from './index.module.scss';

import {createRef} from 'react';
import MailchimpSubscribe from "react-mailchimp-subscribe"

import { Pixel } from '../components/Pixel'


const MailchimpWaitlist = () => {
    const mailchimpUrl =
        'https://outlook.us17.list-manage.com/subscribe/post?u=76de04786b765e0da506aec02&amp;id=825ec8d520';
    const emailRef = createRef();

    return (
        <div>
            <MailchimpSubscribe url={mailchimpUrl} render={({ subscribe, status, message }) => {
            switch (status) {
                case "sending":
                    return <div className={css.waitlistStatusText} >Loading...</div>;
                case "success":
                    return <div className={css.waitlistStatusText} >Success!</div>;
                case "error":
                    return (
                        <form onSubmit={() => {
                            event.preventDefault();
                            subscribe({
                                EMAIL: emailRef.current.value,
                            })
                        }}
                        >
                            <input className={css.waitlistErrorInput}
                                   type="email" ref={emailRef}
                                   placeholder="enter your email"
                            />
                            <input className={css.waitlistSubmitButton} type="submit" value="Get notified on launch!" />
                        </form>
                    );
                default:
                    return (
                        <form onSubmit={() => {
                            event.preventDefault();
                            subscribe({
                                EMAIL: emailRef.current.value,
                            })
                        }}
                        >
                            <input className={css.waitlistInput}
                                   type="email" ref={emailRef}
                                   placeholder="enter your email"
                            />
                            <input className={css.waitlistSubmitButton} type="submit" value="Get notified on launch!" />
                        </form>
                    )
            }
        }}
        />
        </div>
    )
};

export default function Home() {
  return (
  <html lang={"en"} >
      <Head>
        <title>FitChart</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Ecommerce clothing software" />
        <meta name="keywords" content="FitChart, fit chart, clothing, Ecommerce, size chart, online experience" />
        <link rel="icon" href="/favicon.ico" />
        <Pixel />
      </Head>

      <main className={css.mainContainer}>
          <div className={css.header}>
              <img src="/logo.svg" alt="Fitchart logo" className={css.logo} />
          </div>
          <div className={css.colWrapper}>
              <div className={css.leftCol}>
                  <h1 className={css.title}>
                      Increase revenue for your online clothing business by showing your customers how your
                      apparel fits on them, as if they are trying it on in person
                  </h1>

                  <p className={css.description}>
                      Get access to a customizable popup for your online store that lets your customers view how your
                      product fits on others with similar body measurements to help them decide what size they should get.
                  </p>
                  <div className={css.waitlist}>
                    <MailchimpWaitlist />
                  </div>
                  <div className={css.integrations}>
                      <h2 className={css.description} >Integrates with all e-commerce stores, including</h2>
                      <img className={css.integrationLogo} src="/shopify.svg" alt="Shopify logo" width="180" />
                      <img className={css.integrationLogo} src="/woocommerce.svg" alt="Woocommerce logo" width="200" />
                  </div>
              </div>
              <div className={css.rightCol}>
                  <img src="/demo-img-2.png" alt="embedded button screenshot" className={css.screenshot} />
                  <img src="/demo-img.png" alt="app screenshot" className={css.screenshot} />
              </div>
          </div>
      </main>
  </html>
  )
}
