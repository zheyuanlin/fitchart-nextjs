import React from "react";

const SITEMAP_XML = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
      <url>
        <loc>https://fitchart.io/</loc>
        <lastmod>2020-07-30</lastmod>
        <priority>1.00</priority>
      </url>
    </urlset>`;

class Sitemap extends React.Component {
    static async getInitialProps({res}) {
        res.setHeader("Content-Type", "text/xml");
        res.write(SITEMAP_XML);
        res.end();
    }
}

export default Sitemap;